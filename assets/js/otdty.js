// get query string value. Proxy is a newer, and faster javascript method to do this
const query_parameters = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop) => searchParams.get(prop),
});

// grab the query string parameter and show it for people
const transaction_element = document.querySelector("#transactionId");
transaction_element.innerHTML = query_parameters.payment_id;

// show how much they paid
const amount_element = document.querySelector("#transcationAmount");
amount_element.innerHTML = query_parameters.amount;

//const payment_status = document.querySelector("#paymentStatus");
//payment_status.innerHTML = query_parameters.payment_status;