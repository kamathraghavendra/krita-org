---
title: "Krita 5.1.3 正式版已经推出"
date: "2022-11-08"
categories: 
  - "news-zh"
  - "officialrelease-zh"
---

今天我们为大家带来了 Krita 5.1.3 正式版。此版软件修复了一些已知的程序缺陷，更新了 Krita 依赖的程序库，带来了一定的性能提升。我们推荐所有用户更新到此版本。我们跳过了 5.1.2 版本号，因为我们在构建它之后又立即修复了一些程序缺陷。安卓版由于遇到了数字签名问题，依然是 5.1.2 版，它们将受缺陷 [461436](https://bugs.kde.org/show_bug.cgi?id=461436) 和 [459510](https://bugs.kde.org/show_bug.cgi?id=459510) 影响，等待下一版才能修复。

## 已修复程序缺陷列表

本版软件修复了以下程序缺陷：

- Krita 官方构建的软件包：升级 LittleCMS 到最新的发布候补版，它提升了性能和修复了一些程序缺陷。同时更新大量其他 Krita 依赖的程序库。
- 修复在 Plasma 桌面环境下使用发行版提供的软件包的全局菜单。[BUG:408015](https://bugs.kde.org/show_bug.cgi?id=408015)
- JPEG-XL、HEIF：修复导入/导出 HDR 透明度通道。[BUG:460380](https://bugs.kde.org/show_bug.cgi?id=460380)
- JPEG-XL、HEIF：修复标准化 HDR 数值的挤压现象。
- JPEG XL、HEIF：修复 OOTF 的保存问题。
- JPEG XL：修复 HLG 修正选项的启用问题。
- JPEG-XL：为带有未定义转移函数的线性特性文件提供应对方案。
- JPEG-XL：优化 HDR 导出。
- JPEG-XL：改进与 Chrome 当前的实验性 JPEG-XL 支持。
- 修复当剪贴板为空时从剪贴板创建图像。[BUG:459800](https://bugs.kde.org/show_bug.cgi?id=459800)
- 修复 CSV 动画文件的加载。
- 修复在图像边缘外面粘贴。[BUG:459111](https://bugs.kde.org/show_bug.cgi?id=459111)
- 修复小口径笔刷的笔尖图像锯齿。
- 修复直线工具的一些问题。[BUG:460461](https://bugs.kde.org/show_bug.cgi?id=460461)
- 修复在新建图像中建立选区并剪切/复制时的崩溃。[BUG:457475](https://bugs.kde.org/show_bug.cgi?id=457475), BUG:460954
- 安卓版本：修复长按触发右键点击事件。
- 安卓版本：为镜像模式装饰显示处理触摸事件。
- 修复图案填充图层的一处崩溃问题。[BUG:459906](https://bugs.kde.org/show_bug.cgi?id=459906)
- 修复矢量对象的前景色到背景色切换。[BUG:458913](https://bugs.kde.org/show_bug.cgi?id=458913)
- 修复 TIFF 图像导出的数个问题。[BUG:459840](https://bugs.kde.org/show_bug.cgi?id=459840)
- 修复更换配色主题时的一些问题。
- 修复保存有极端宽高比的图像。[BUG:460624](https://bugs.kde.org/show_bug.cgi?id=460624)
- 修复路径选择工具的一些问题。
- 实现折线工具对所添加的点的右键撤销。
- 修复对动画图层的复制/粘贴。[BUG:457319](https://bugs.kde.org/show_bug.cgi?id=457319), [BUG:459763](https://bugs.kde.org/show_bug.cgi?id=459763)
- 实现一次导入多个资源包。
- 实现在启用了 SELinux 的 Linux 环境下运行 Krita。[BUG:459490](https://bugs.kde.org/show_bug.cgi?id=459490)
- 修复在最近图像列表中包含有带有图层样式的 PSD 图像时，Krita 会在启动时崩溃的问题。[BUG:459512](https://bugs.kde.org/show_bug.cgi?id=459512)
- 实现在没有可绘制图层时依然可以运行 Python 脚本。[BUG:459495](https://bugs.kde.org/show_bug.cgi?id=459495)
- 修复常用脚本插件对所选脚本的记忆。[BUG:421231](https://bugs.kde.org/show_bug.cgi?id=421231)
- 为 PNG 图像导出功能增加在导出时转换为 8 位图像的选项。[BUG:459415](https://bugs.kde.org/show_bug.cgi?id=459415)
- 修复在高分辨率模式启用时，在参考图像上悬停会造成显示错误的问题。[BUG:441216](https://bugs.kde.org/show_bug.cgi?id=441216)
- 确保穿透图层不会创建缩略图。[BUG:440960](https://bugs.kde.org/show_bug.cgi?id=440960)
- 使 OpenGL 问题临时应对方案在全平台上可用。[BUG:401940](https://bugs.kde.org/show_bug.cgi?id=401940)
- PSD 图像支持：修复图像混合范围的读取。[BUG:459307](https://bugs.kde.org/show_bug.cgi?id=459307)
- 修复大量的内存泄漏问题。
- 修复在使用移动工具后的复制-粘贴操作。[BUG:458764](https://bugs.kde.org/show_bug.cgi?id=458764)
- 在主菜单的帮助->系统信息对话框中显示所有已加载的 Python 插件。
- 在保存参考图像集时显示忙碌光标。[BUG:427546](https://bugs.kde.org/show_bug.cgi?id=427546)
- 添加 Document::setModified 到脚本 API。[BUG:425066](https://bugs.kde.org/show_bug.cgi?id=425066)
- 修复在保存包含填充图层的图像时的一处崩溃。[BUG:459252](https://bugs.kde.org/show_bug.cgi?id=459252)
- 修复复制/粘贴矢量形状、填充图层、选区蒙版时的一处崩溃。[BUG:458115](https://bugs.kde.org/show_bug.cgi?id=458115)
- 修复加载 512x512 PSD 图像时的图层缩略图。[BUG:458887](https://bugs.kde.org/show_bug.cgi?id=458887)
- 修复复制/粘贴背景图层时的一处崩溃。[BUG:458890](https://bugs.kde.org/show_bug.cgi?id=458890),[458857](https://bugs.kde.org/show_bug.cgi?id=458857),[458248](https://bugs.kde.org/show_bug.cgi?id=458248),[458941](https://bugs.kde.org/show_bug.cgi?id=458941)
- 鼠标悬停在带有颜色标签的图层时不高亮显示该图层。[BUG:459153](https://bugs.kde.org/show_bug.cgi?id=459153)
- 修复在创建文件名包含 \[ 和 \] 半角方括号时的带编号备份文件。[BUG:445500](https://bugs.kde.org/show_bug.cgi?id=445500)
- 添加透视变形的中间控制点 (感谢 Carsten Hartenfels 提供补丁)
- 修复在笔刷轮廓预览和吸附在禁用时锐利度选项的笔尖印迹不精确的问题。[BUG:458361](https://bugs.kde.org/show_bug.cgi?id=458361)
- 修复 macOS 下的触摸板手势。[BUG:456446](https://bugs.kde.org/show_bug.cgi?id=456446)

## 下载

### 中文版信息

- 中文支持：Krita 的所有软件包均内建中文支持，首次安装时会自动设置为操作系统的语言。
- 手动设置：菜单栏 --> Settings --> Switch Application Language (倒数第二项) --> 下拉选单 --> 中文 (底部)，重启程序生效。
- G'MIC 滤镜中文翻译：打开 G'MIC 滤镜 -> 设置 (左下角) -> 勾选“Translate Filters”。

### Windows 版本

- **64 位 Windows 安装程序** [本站下载](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3-setup.exe) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位 Windows 免安装包** [本站下载](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位程序崩溃调试包** [本站下载](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3-dbg.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)

- **配套资源** [中文离线文档](https://share.weiyun.com/Dea2uj0M) | [FFmpeg 软件包](https://share.weiyun.com/HzkXn7YS) | [Krita 配置文件和资源清理工具](https://share.weiyun.com/SCCloC47)

- 32 位支持：最后一版支持 32 位 Windows 的 Krita 为 4.4.3，[本站下载](https://download.kde.org/stable/krita/4.4.3/krita-x86-4.4.3-setup.exe) | [网盘下载](https://share.weiyun.com/wdMnx1WB)。
- 免安装包：解压到任意位置，运行目录中的 Krita 快捷方式。不带文件管理器缩略图插件，与已安装版本共用配置文件和资源，但程序本身相互独立。
- 程序崩溃调试包：解压到 Krita 的安装目录，在报告程序崩溃问题时用于获取回溯追踪数据。日常使用无需下载此包。

### Linux 版本

- **64 位 Linux AppImage 程序包** [本站下载](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3-x86_64.appimage) | [网盘下载](https://share.weiyun.com/j7Vrjx2m)
- G'Mic 插件已经整合到主程序包中
- 如果浏览器把链接作为文本打开，请右键点击链接另存为文件。

### macOS 版本

- **macOS 程序映像** [本站下载](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.dmg) | [网盘下载](https://share.weiyun.com/jc82ykle)

- 如果您还在使用 OSX Sierra 或者 High Sierra，请[观看此视频](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何启动由开发人员签名的可执行软件包。

### 安卓版本

- **64 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.2/krita-x86_64-5.1.2-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **32 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.2/krita-x86-5.1.2-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **64 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.2/krita-arm64-v8a-5.1.2-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- **32 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.2/krita-armeabi-v7a-5.1.2-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- Krita 在 ChromeOS 下已经可以稳定用于生产用途，但在一般安卓系统下仍处于测试阶段。
- 安卓版的整体功能与桌面版本几乎完全相同，它尚未对手机等屏幕狭长的设备进行优化，也尚未开发平板专属的模式。建议搭配键盘使用。
- 近期上市的安卓平板一般采用 ARM CPU，Chromebook 等上网本一般采用 Intel CPU。
- 请先尝试 64 位的两种安装包，不行的话再尝试 32 位的两种。

### 源代码

- [TAR.GZ 格式源代码包](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.tar.gz)
- [TAR.XZ 格式源代码包](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.tar.xz)

### md5sum 校验码

适用于上述所有软件包，用于校验下载文件的完整性。如果您不了解文件校验，忽略即可：

- 请访问[下载目录页面](https://download.kde.org/stable/krita/5.1.3)，点击最右列的“Details”获取哈希值。

### 文件完整性验证密钥

Linux 的 Appimage 可执行文件包和源代码的 .tar.gz 和 .tar.xz tarballs 压缩包已经经过数字签名。您可以[在此下载公钥](https://files.kde.org/krita/4DA79EDA231C852B)，还可以在此下载[数字签名的 SIG 文件](https://download.kde.org/stable/krita/5.1.3/)。
