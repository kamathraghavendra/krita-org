---
title: "First Beta for Krita 5.2.3 Released"
date: "2024-06-05"
categories: 
  - "development-builds"
---

We are releasing the first Beta of Krita 5.2.3. This release primarily brings a complete overhaul of our build system, making it so that our CI system can now build for all 4 platforms (a Continuous Integration system basically builds a program after every change, runs some tests and based on that helps us track down mistakes we made when changing Krita's code).

Beyond the rework of the build system, this release also has numerous fixes, particularly with regards to animated transform masks, jpeg-xl support, shortcut handling on Windows and painting assistants.

In addition to the core team, special thanks goes out to to Freya Lupen, Grum 999, Mathias Wein, Nabil Maghfur Usman, Alvin Wong, Deif Lou and Rasyuqa A. H. for various fixes, as well as the first time contributors in this release cycle (Each mentioned after their contribution).

- Fix docker ShowOnWelcomePage flag ([Bug 476683](https://bugs.kde.org/show_bug.cgi?id=476683))
- windows: Exclude meson in release package
- Fix integer division rounding when scaling image (Patch by David C. Page, Thanks!)
- Fix Comic Manager export with cropping TypeError
- WebP: properly export color profile on RGBA model ([Bug 477219](https://bugs.kde.org/show_bug.cgi?id=477219))
- WebP: properly assign profile when importing image with non-RGBA model ([Bug 478307](https://bugs.kde.org/show_bug.cgi?id=478307))
- Fix saving animated webp
- Added missing layer conversion options to layer docker rightclick menu (Patch by Ken Lo, thanks! [MR 2039](https://invent.kde.org/graphics/krita/-/merge_requests/2039)
- Fix Last Documents Docker TypeError
- Replace the Backers tab with a Sponsors tab
- Comic Project Management Tools: Fix crash in comic exporter due incorrect api usage. ([Bug 478668](https://bugs.kde.org/show_bug.cgi?id=478668)) Also turn off metadata retrieval for now.
- Properly check color patch index for validity ([Bug 480188](https://bugs.kde.org/show_bug.cgi?id=480188))
- Fix build with libjxl 0.9.0 ([Bug 478987](https://bugs.kde.org/show_bug.cgi?id=478987), patch by Timo Gurr, thanks!)
- Included Krita's MLT plugin in our source tree, as well as several fixes to it.
- Possibly fix ODR violation in the transform tool strategies ([Bug 480520](https://bugs.kde.org/show_bug.cgi?id=480520))
- Fix framerate issue when adding audio to the image ([Bug 481388](https://bugs.kde.org/show_bug.cgi?id=481388))
- Fix artifacts when drawing on a paused animation ([Bug 481244](https://bugs.kde.org/show_bug.cgi?id=481244))
- Fix issues generating reference image in KisMergeLabeledLayersCommand ([Bug 471896](https://bugs.kde.org/show_bug.cgi?id=471896), [Bug 472700](https://bugs.kde.org/show_bug.cgi?id=472700))
- Simplify similar color selection tool and add spread option
- Fix artifacts when moving a layer under a blur adjustment layer ([Bug 473853](https://bugs.kde.org/show_bug.cgi?id=473853))
- Make sure that tool-related shortcuts have priority over canvas actions ([Bug 474416](https://bugs.kde.org/show_bug.cgi?id=474416))
- Refactor KisLodCapableLayerOffset into a reusable structure
- Fix current time loading from a .kra file with animation
- Clone scalar keyframes when creating a new one in the GUI
- Refactor transform masks not to discard keyframe channels on every change ([Bug 475385](https://bugs.kde.org/show_bug.cgi?id=475385))
- Fix artifacts when flattening animated transform mask ([Bug 475334](https://bugs.kde.org/show_bug.cgi?id=475334))
- Fix extra in-stroke undo step when using Transform and Move tools ([Bug 444791](https://bugs.kde.org/show_bug.cgi?id=444791))
- Fix rounding in animated transform masks with translation ([Bug 456731](https://bugs.kde.org/show_bug.cgi?id=456731))
- Fix canvas inputs breaking due to inconsistent key states (BUG: 451424)
- Make wave filter allowed in filter masks/layers ([Bug 476033](https://bugs.kde.org/show_bug.cgi?id=476033))
- Fix an offset when opening files with animated transform masks ([Bug 476317](https://bugs.kde.org/show_bug.cgi?id=476317))
- Fix transform mask offsets after running a transform tool on that ([Bug 478966](https://bugs.kde.org/show_bug.cgi?id=478966))
- Fix creation of keyframes **before** the first frame for scalar channels ([Bug 478448](https://bugs.kde.org/show_bug.cgi?id=478448))
- Support xsimd 12 ([Bug 478986](https://bugs.kde.org/show_bug.cgi?id=478986))
- Fix new scalar frames to use interpolated values ([Bug 479664](https://bugs.kde.org/show_bug.cgi?id=479664))
- Don't skip updates on colorize mask props change ([Bug 475927](https://bugs.kde.org/show_bug.cgi?id=475927))
- Fix restoring layer's properties after new-layer-from-visible action ([Bug 478225](https://bugs.kde.org/show_bug.cgi?id=478225))
- Fix transform masks being broken after duplication ([Bug 475745](https://bugs.kde.org/show_bug.cgi?id=475745))
- Fix a crash when trying to open multiple files with one D&D ([Bug 480331](https://bugs.kde.org/show_bug.cgi?id=480331))
- Fix cache being stuck when adding hold frames ([Bug 481099](https://bugs.kde.org/show_bug.cgi?id=481099))
- Make PerspectiveAssistant respect snapToAny parameter
- Fix logic in 2-Point Perspective Assistant
- Remove unnecessary locking code from several assistants
- Move a bunch of code repeated in derived classes to KisPaintingAssistant
- Fix wg selector popup flickering with shortcut
- Fix updating KoCanvasControllerWidget's canvas Y offset when resizing the canvas
- Fix esthetics of the Welcome Page, remove banner.
- Re-enable the workaround for plasma global menu ([Bug 483170](https://bugs.kde.org/show_bug.cgi?id=483170))
- Improvements to support for Haiku OS.
- Fix crash on KisMergeLabeledLayersCommand when using color labeled transform masks ([Bug 480601](https://bugs.kde.org/show_bug.cgi?id=480601))
- PNG: prevent multiple color conversions ([Bug 475737](https://bugs.kde.org/show_bug.cgi?id=475737))
- Fix crash when pasting font family in TextEditor ([Bug 484066](https://bugs.kde.org/show_bug.cgi?id=484066))
- Fix grids glitches when subdivision style is solid ([Bug 484889](https://bugs.kde.org/show_bug.cgi?id=484889))
- Add 1px padding to rects in KisPerspectiveTransformWorker to account for bilinear filtering ([Bug 484677](https://bugs.kde.org/show_bug.cgi?id=484677))
- Fix activation of the node on opening .kra document ([Bug 480718](https://bugs.kde.org/show_bug.cgi?id=480718))
- Various snapcraft fixes by Scarlet Moore
- Fix sequence of imagepipe brushes when used in the line tool ([Bug 436419](https://bugs.kde.org/show_bug.cgi?id=436419))
- tilt is no longer ignored in devices that support tilt ([Bug 485709](https://bugs.kde.org/show_bug.cgi?id=485709), patch by George Gianacopoulos, thanks!)
- Make Ogg render consistently check for Theora
- Fix issue with appbundle builder using libs.xml from arm64-v8a ([Bug 485707](https://bugs.kde.org/show_bug.cgi?id=485707))
- Don't reset redo when doing color picking ([Bug 485910](https://bugs.kde.org/show_bug.cgi?id=485910))
- Show and error when trying to import version 3 xcf files ([Bug 485420](https://bugs.kde.org/show_bug.cgi?id=485420))
- Remove a bogus warning about Intel's openGL driver
- Fix incorrect inclusion of "hard mix softer" blend mode in layer styles
- Fixed issue on S24 ultra regarding S-pen settings when HiDPI is turned off (Patch by Spencer Sawyer, thanks!).
- Revive code path in perspective ellipse and 2pp assistant draw routine
- Don't try to regenerate frames on an image that has no animation
- Fix data loss on reordering storyboards.
- Improving the quality of icons for Android (Patch by Jesse 205, thanks! [Bug 463043](https://bugs.kde.org/show_bug.cgi?id=463043))
- Enable SIP type stub file generation (Patch by Kate Corcoran, thanks!)

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-5.2.3-beta1-setup.exe](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-5.2.3-beta1-setup.exe)
- Portable 64 bits Windows: [krita-5.2.3-beta1.zip](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-5.2.3-beta1.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-5.2.3-beta1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.3-beta1-x86\_64.appimage](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-5.2.3-beta1-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.2.3-beta1.dmg](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-5.2.3-beta1.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen.

We are no longer building the `32 bits Intel CPU APK`, as we suspect it was only installed by accident and none of our users actually used it. We are hoping for feedback on this matter.

- [64 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-x86_64-5.2.3-beta1-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-arm64-v8a-5.2.3-beta1-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-armeabi-v7a-5.2.3-beta1-release-signed.apk)

### Source code

- [krita-5.2.0-beta1.tar.gz](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-5.2.3-beta1.tar.gz)
- [krita-5.2.0-beta1.tar.xz](https://download.kde.org/unstable/krita/5.2.3-beta1/krita-5.2.3-beta1.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/unstable/krita/5.2.3-beta1/](https://download.kde.org/unstable/krita/5.2.3-beta1) and click on Details to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/dmitry_kazakov.gpg). The signatures are [here](https://download.kde.org/unstable/krita/5.2.3-beta1/) (filenames ending in .sig).