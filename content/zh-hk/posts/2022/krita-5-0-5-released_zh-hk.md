---
title: "發佈 Krita 5.0.5 修正版本"
date: "2022-04-14"
categories: 
  - "news_zh-hk"
  - "officialrelease_zh-hk"
---

今日我哋發佈 Krita 5.0.5。如無意外，今次將會係我哋喺發佈 Krita 5.1.0 之前（目前希望能夠喺六月達成）最後一個 5.0 修正版本。對上一個修正版本係 5.0.2。由於喺更新個別商店平台嗰陣佔用咗 5.0.3 同 5.0.4 依兩個版本編號，因此呢次修正版本要直接跳到 5.0.5 喇。

今次更新包含咗以下修正： （譯者按：由於內容太多而人力資源有限，所以依段唔做完整嘅翻譯，保留返英文原文。）

- Fix artefacts when using the color smudge brush on CMYK images. [BUG:447211](https://bugs.kde.org/show_bug.cgi?id=447211)
- Fix blurry results in 4-point perspective transform. [BUG:447255](https://bugs.kde.org/show_bug.cgi?id=447255)
- Fix undo of liquify transform properties. [BUG:447314](https://bugs.kde.org/show_bug.cgi?id=447314)
- Fix the size of the About Krita dialog.
- Fix a crash when changing the Instant Preview settings of a brush preset.
- Fix use of layer names when using the G'Mic plugin. [BUG:447293](https://bugs.kde.org/show_bug.cgi?id=447293), [BUG:429851](https://bugs.kde.org/show_bug.cgi?id=429851)
- Add missing blending modes for G'Mic filters. [BUG:447293](https://bugs.kde.org/show_bug.cgi?id=447293)
- Fix issues with resource libraries with upper-case filename extensions. [BUG:447454](https://bugs.kde.org/show_bug.cgi?id=447454)
- Enable the OS file dialog on macOS by default.
- Fix broken macOS Arm64 canvas rendering for 16 bit/integer images.
- Fix crashes when undoing multiple layer operations too quickly. [BUG:447462](https://bugs.kde.org/show_bug.cgi?id=447462)
- Work around a crash in the transform mask applied to a passthrough group. [BUG:447506](https://bugs.kde.org/show_bug.cgi?id=447506)
- Fix the photoshop compatible shortcuts. [BUG:447771](https://bugs.kde.org/show_bug.cgi?id=447771)
- Fix AppimageUpdate. [BUG:446757](https://bugs.kde.org/show_bug.cgi?id=446757)
- Show the correct number of layers in the Image Properties dialog. Patch by Dan McCarthy, thanks!
- Fix the layout of the docker titlebars.
- Disable automatically assigning accelerator keys for selecting dockers.
- Fix a memory leak in the color history display.
- Fix a race condiction in the metadata system initialization.
- Fix animation playback when having multiple views on an animation. [BUG:450425](https://bugs.kde.org/buglist.cgi?quicksearch=450425)
- Fix dataloss when scaling an image with animated transform masks. [BUG:450781](https://bugs.kde.org/show_bug.cgi?id=450781)
- Fix incorrect basename for an animation when filepath or filename changes. [BUG:451654](https://bugs.kde.org/show_bug.cgi?id=451654)
- Fix restoring the default values for canvas input settings. [BUG:396064](https://bugs.kde.org/show_bug.cgi?id=396064)
- Update gradient colors correctly in layer styles. [BUG:452066](https://bugs.kde.org/show_bug.cgi?id=452066)
- Fix a crash when handling touch events.
- Fix a large number of defects found by Coverity.
- Fix the hitboxes in the curve widget being too small on HiDPI screens. [BUG:446755](https://bugs.kde.org/show_bug.cgi?id=446755)
- Fix saving over deactivated resources.
- Fix the Advanced Color Selector's hsySH square element. [BUG:452422](https://bugs.kde.org/show_bug.cgi?id=452422)
- Fix issues with cursors being too small on HiDPI screens. [BUG:448107](https://bugs.kde.org/show_bug.cgi?id=448107), [BUG:447314](https://bugs.kde.org/show_bug.cgi?id=447314)
- Fix problems with animating transparency and filter masks. [BUG:452170](https://bugs.kde.org/show_bug.cgi?id=452170)
- Opacity now loads correctly for animated vector layers. [BUG:452144](https://bugs.kde.org/show_bug.cgi?id=452144)
- Stop the storyboard docker creating frames on locked layers. [BUG:447396](https://bugs.kde.org/show_bug.cgi?id=447396)
- Storyboards: add the ability to duplicate existing scenes.
- Improve the interaction with ffmpeg when rendering animations or recordings.
- Improve detection of broken video sequences on import as animation.
- Work around a problem where not all pixels got copied when copying a mask. [BUG:453164](https://bugs.kde.org/show_bug.cgi?id=453164)
- Make it possible to save and load gradients on non-UTF8 locales. [BUG:447730](https://bugs.kde.org/show_bug.cgi?id=447730)
- Fix issue with masks and layers being broken after changing the colorspace.
- Update to LittleCMS 2.13.1 to fix a bug in the grayscale colorspace and fix conversion between 8 bits and 16 bits grayscale images. [BUG:447484](https://bugs.kde.org/show_bug.cgi?id=447484)
- Fix a delay on autobrush strokes with randomness: [BUG:446663](https://bugs.kde.org/show_bug.cgi?id=446663)
- Improve performance when moving a layer in a layer group. [BUG:450957](https://bugs.kde.org/show_bug.cgi?id=450957)
- Fix brush outline precision for pixel-art brushes. [BUG:447466](https://bugs.kde.org/show_bug.cgi?id=447466)
- Improve the brush outline for low-density brushes. [BUG:447045](https://bugs.kde.org/show_bug.cgi?id=447045)
- Set .apng as the default file extension for APNG files instead of PNG. [BUG:451473](https://bugs.kde.org/show_bug.cgi?id=451473)
- Fix crash when using the smart patch tool on HDR images. [BUG:451912](https://bugs.kde.org/show_bug.cgi?id=451912)
- Make it possible to set Weighted Distance to values larger than 100. [BUG:451874](https://bugs.kde.org/show_bug.cgi?id=451874)
- Fix the the fuzziness setting for the contiguous selection tool. [BUG:447524](https://bugs.kde.org/show_bug.cgi?id=447524)
- Fix the default eraser preset. [BUG:447650](https://bugs.kde.org/show_bug.cgi?id=447650)
- Add missing shortcuts for the following filters: slope/offset/power, cross-channel adjustment curves, halftone, gaussian high pass, height to normal map, gradient map, normalize and palettize. [BUG:451337](https://bugs.kde.org/show_bug.cgi?id=451337)
- Fix saving new workspaces. [BUG:446985](https://bugs.kde.org/show_bug.cgi?id=446985)
- Correctly handle the ICC profile blacklist.
- Fix the "Photoshop signature verification failed!" warning if there are zero sized blocks in the PSD file. [BUG:450983](https://bugs.kde.org/show_bug.cgi?id=450983)
- Make starting Krita more robust if the configuration file is corrupted. [BUG:449983](https://bugs.kde.org/show_bug.cgi?id=449983)
- Add pressure and rotation to touch support.
- Fix translation issues on Android. [BUG:448343](https://bugs.kde.org/show_bug.cgi?id=448343)
- Fix autosaving on Android.
- Handle flags in SVG paths in vector layers correctly: [BUG:447417](https://bugs.kde.org/show_bug.cgi?id=447417)
- Fix crash when the selected gradient isn't shown in the gradient selector in the Layer Style dialog. Similar for patterns. [BUG:448296](https://bugs.kde.org/show_bug.cgi?id=448296), [BUG:445922](https://bugs.kde.org/show_bug.cgi?id=445922)
- Fix an issue in the brush preset selector if no brush preset has been selected. [BUG:449226](https://bugs.kde.org/show_bug.cgi?id=449226), [BUG:450121](https://bugs.kde.org/show_bug.cgi?id=450121)
- Fix a bug in Qt's accessibility handling. [BUG:449122](https://bugs.kde.org/show_bug.cgi?id=449122)
- Fix the menubar height in MDI mode with maximized subwindow on HiDPI screens. [BUG:449118](https://bugs.kde.org/show_bug.cgi?id=449118)
- Fix the speed sensor in the line tool. [BUG:434488](https://bugs.kde.org/show_bug.cgi?id=434488)
- Fix a crash in the raindrops filter when applied to an empty layer. [BUG:449408](https://bugs.kde.org/show_bug.cgi?id=449408)
- Fix a race condition in Select All. [BUG:449122](https://bugs.kde.org/show_bug.cgi?id=449122)
- Improve thread handling when painting, improving performance and energy efficiency. [BUG:367901](https://bugs.kde.org/show_bug.cgi?id=367901), [BUG:360677](https://bugs.kde.org/show_bug.cgi?id=360677)
- Improve the performance of the resource selectors if there are many resource libraries installed.
- Fix issues with saving tags when updating the database schema.
- Make it possible to tag and untag multiple resources at once.
- Reset the painting assistant after each stroke. [BUG:448187](https://bugs.kde.org/show_bug.cgi?id=448187)
- Fix issues witht he color smudge radius range. [BUG:441682](https://bugs.kde.org/show_bug.cgi?id=441682)
- Remove broken resource files after importing them fails. [BUG:446279](https://bugs.kde.org/show_bug.cgi?id=446279)
- Fix reference image update on resizing in HiDPI. [BUG:430988](https://bugs.kde.org/show_bug.cgi?id=430988)
- Fix issue with MyPaint presets when Slow Tracking interacts with the Stabilizer. [BUG:447562](https://bugs.kde.org/show_bug.cgi?id=447562)
- Fix banding when rendering in HDR mode. [BUG:445672](https://bugs.kde.org/show_bug.cgi?id=445672)
- Fix rendering SVG file layers. [BUG:448256](https://bugs.kde.org/show_bug.cgi?id=448256)
- Fix the small color selector on OpenGL 2.1. [BUG:447868](https://bugs.kde.org/show_bug.cgi?id=447868)
- Fix using the default theme if there is no theme configured. [BUG:448483](https://bugs.kde.org/show_bug.cgi?id=448483)
- Fix sizing of preview images in the non-native file dialog. [BUG:447805](https://bugs.kde.org/show_bug.cgi?id=447805)
- Improve handling of the CTRL modifier key. [BUG:438784](https://bugs.kde.org/show_bug.cgi?id=438784)
- Fix update of brush outline when switching brush preset or tool. [BUG:428988](https://bugs.kde.org/show_bug.cgi?id=428988), [BUG:442343](https://bugs.kde.org/show_bug.cgi?id=442343)
- Improve handling the thumbnails for the recent files list.
- Fix setting the configuration of a fill layer from the scripting API. [BUG:447807](https://bugs.kde.org/show_bug.cgi?id=447807)

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 係自由、免費同開源嘅專案。請考慮[加入 Krita 發展基金](https://fund.krita.org/)、[損款](https://krita.org/en/support-us/donations/)，或者[購買教學影片](https://krita.org/en/shop/)支持我哋啦！有你哋嘅熱心支持，我哋先可以俾核心開發者全職為 Krita 工作。

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會同安裝版本共用設定檔同埋資源。如果想用免安裝版測試並回報 crash 嘅問題，請同時下載偵錯符號 (debug symbols)。

注意：我哋依家唔再提供為 32 位元 Windows 建置嘅版本。

- 64 位元安裝程式：[krita-x64-5.0.5-setup.exe](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5-setup.exe)
- 64 位元免安裝版：[krita-x64-5.0.5.zip](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5.zip)
- [偵錯符號 Debug symbols（請解壓到 Krita 程式資料夾入面）](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5-dbg.zip)

### Linux

- 64 位元 Linux AppImage：[krita-5.0.5-x86\_64.appimage](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5-x86_64.appimage)

Linux 版本依家唔使再另外下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你用緊 macOS Sierra 或者 High Sierra，請睇下[依段影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解點樣執行開發者簽署嘅程式。

- macOS 套件：[krita-5.0.5.dmg](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.dmg)

### Android

我哋提供嘅 ChomeOS 同 Android 版本仲係**測試版本**。依個版本或可能含有大量嘅 bug，而且仲有部份功能未能正常運作。由於使用者介面仲未改進好，軟件或者須要配合實體鍵盤先可以用到全部功能。Krita 唔啱俾 Android 智能電話用，只係啱平板電腦用，因為使用者介面嘅設計並未為細小嘅螢幕做最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.0.5/krita-x86_64-5.0.5-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.0.5/krita-x86-5.0.5-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.0.5/krita-arm64-v8a-5.0.5-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.0.5/krita-armeabi-v7a-5.0.5-release-signed.apk)

### 原始碼

- [krita-5.0.5.tar.gz](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.tar.gz)
- [krita-5.0.5.tar.xz](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.tar.xz)

### md5sum

下載檔案嘅 MD5 校對碼可以喺依個檔案入面揾到：

- [md5sum.txt](https://download.kde.org/stable/krita/5.0.5/md5sum.txt)

### 數碼簽署

Linux AppImage 以及原始碼嘅 .tar.gz 同 .tar.xz 壓縮檔已使用數碼簽署簽名。你可以由[依度](https://files.kde.org/krita/4DA79EDA231C852B)取得 public key。簽名檔可以喺[依度](https://download.kde.org/stable/krita/5.0.5/)揾到（副檔名為 .sig）。
